<?php

namespace Tests\Unit\Url;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserTest extends TestCase
{
  use RefreshDatabase;

  public function testSalvarUsuarioSimplesSucesso()
  {
    $this->post('/users', [
      'id' => 'user',
    ])->assertStatus(Response::HTTP_CREATED);
  }

  public function testSalvarDoisUsuariosIguaisErro()
  {
    $this->post('/users', [
      'id' => 'user',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->post('/users', [
      'id' => 'user',
    ])->assertStatus(Response::HTTP_CONFLICT);
  }

  public function testRetornaEstatisticaUrlsUsuarioSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->post('/users/joao/urls', [
      'url' => 'http://www.google.com',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->get('/users/joao/stats')->assertStatus(Response::HTTP_OK);
  }

  public function testApagarUsuarioInformadoSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->delete("/users/joao")->assertStatus(Response::HTTP_NO_CONTENT);
  }
}
