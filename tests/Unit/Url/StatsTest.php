<?php

namespace Tests\Unit\Url;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class StatsTest extends TestCase
{
  use RefreshDatabase;

  public function testRetornaEstatisticasSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->post('/users/joao/urls', [
      'url' => 'http://www.google.com',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->get('/stats')
      ->assertStatus(Response::HTTP_OK)
      ->assertJsonStructure([
        'hits',
        'urlCount',
        'topUrls' => [
          [
            'id',
            'hits',
            'url',
            'shortUrl',
          ]
        ],
      ]);
  }

  public function testRetornaEstatisticasUrlSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $response = $this->post('/users/joao/urls', [
      'url' => 'http://www.google.com',
    ])->assertStatus(Response::HTTP_CREATED)->baseResponse->getContent();

    $response = json_decode($response);

    $this->get("/stats/{$response->id}")
      ->assertStatus(Response::HTTP_OK)
      ->assertJsonStructure([
        'id',
        'hits',
        'url',
        'shortUrl',
      ]);
  }
}