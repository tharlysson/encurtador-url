<?php

namespace Tests\Unit\Url;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class UrlTest extends TestCase
{
  use RefreshDatabase;

  public function testRedirecionarUrlErro()
  {
    $response = $this->get('/HAHAHA');
    $response->assertStatus(Response::HTTP_NOT_FOUND);
  }

  public function testRedirecionarUrlSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $response = $this->post('/users/joao/urls', [
      'url' => 'http://www.google.com',
    ])->assertStatus(Response::HTTP_CREATED)->baseResponse->getContent();
    $response = json_decode($response);

    $this->get($response->shortUrl)->assertStatus(Response::HTTP_MOVED_PERMANENTLY);
  }

  public function testSalvarUrlSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $this->post('/users/joao/urls', [
      'url' => 'http://www.google.com',
    ])->assertStatus(Response::HTTP_CREATED)
      ->assertJsonStructure([
        'hits',
        'id',
        'shortUrl',
        'url'
      ]);
  }

  public function testApagarUrlSistemaSucesso()
  {
    $this->post('/users', [
      'id' => 'joao',
    ])->assertStatus(Response::HTTP_CREATED);

    $response = $this->post('/users/joao/urls', [
      'url' => 'http://www.google.com',
    ])->assertStatus(Response::HTTP_CREATED)->baseResponse->getContent();
    $response = json_decode($response);

    $this->delete("/urls/{$response->id}")->assertStatus(Response::HTTP_NO_CONTENT);
  }
}