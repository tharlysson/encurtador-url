deploy_prod:
	docker build -t "tharlysson:encurtador" .
	docker stack deploy --compose-file docker-compose.yaml url

test:
	bash -x script/test.sh

migrate:
	bash -x script/migrate.sh