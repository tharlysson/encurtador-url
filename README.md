## Desafio Encurtador de URL

Este projeto é uma API RESTful para um encurtador de URLs.  

Ele foi desenvolvido utilizando o **Laravel Framework** e seu padrão de desenvolvimento, para persistência dos dados foi utilizado o **MySQL**. O projeto foi desenvolvido para atender o requisito de stateless, ou seja cada acesso do usuário é como se fosse o primeiro não salvando nenhum estado para futuros acessos.
  
Outro requisito da aplicação é que ela seja escalável, para atender foi utilizado o Docker Swarm com o intuito de deixar o serviço Serverless, fazendo com que o docker gerencie as requisições e as distribua entre os containers que estão executando a aplicação, não sobrecarregando uma única instância do serviço.


## Dependências do Projeto

Para poder executar o projeto é necessário as seguintes dependências:

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- Make

## Instalação

Para executar a instalação da aplicação basta seguir os passos abaixo:

- Habilitar o Swarm mode em seu docker utilizando o seguinte comando   
`$ docker swarm init`  
Caso você possua mais de uma interface de rede você pode definir o IP que o swarm irá executar os serviços, por exemplo:  
`$ docker swarm init --advertise-addr 192.168.25.10`

- Para instalar o projeto você deverá clonar este repositório  

- Entrar na pasta do projeto e executar o seguinte comando:  
`$ make deploy_prod`   
Este comando cria uma imagem contendo o código do projeto e a configuração para poder executa-lo. 

**ATENÇÃO**: Este projeto foi configurado para subir 3 instâncias da aplicação para poder aplicar o load balance, caso você queira alterar a quantidade, basta mudar a quantidade de replicas no arquivo **docker-compose.yaml**  
`replicas: 3` 

- Após subir o serviço das aplicações e do banco de dados, é necessário esperar até que o serviço esteja completamente ativo, você deverá executar as migrations para que seja criada a estrutura do banco de dados. Basta executar o seguinte comando.  
`$ make migrate`  
Após a execução você já poderá utilizar o serviço normalmente. 


## Testes
Caso deseje executar testes unitários nos serviços fornecidos por esta aplicação basta executar o comando:  
`$ make test`

## Endpoints
Para acessar os endpoints você deverá seguir o seguinte padrão:  
`http://<host>:8080/<endpoint>`  
Caso deseje retirar a porta, antes de buildar o projeto cofigure a porta de sua preferência no **docker-composer.yaml**  



## `GET /:id`

Retorna um 301 redirect para o endereço original da URL.

```
301 Redirect
Location:
```
Caso o `id` não existe no sistema, o retorno deverá é um `404 Not Found` .


## `POST /users`

Cadastra um novo usuário no sistema.

```
{
    "id": "joao"
}
```
Irá retornar um `201 Created` em caso de sucesso. Caso já exista um usuário com
o mesmo id retornar código `409 Conflict` .

## `POST /users/:userid/urls`
Cadastra uma nova url no sistema.
```
{
    "url": "http://www.example.com/blog"
}
```
A resposta será um objeto JSON igual ao da chamada `GET /stats/:id` com código `201 Created` .
```
{
    "hits": 0,
    "id": "23094",
    "shortUrl": "http://<host>[:<port>]/asdfeiba",
    "url": "http://www.example.com/blog"
}
```

## `GET /users/:userId/stats`
Retorna estatísticas das urls de um usuário. O resultado é o mesmo que `GET /stats` mas com o escopo dentro de um usuário.  

Caso o usuário não exista o retorno será com código `404 Not Found` .

## `DELETE /urls/:id`
Apaga uma URL do sistema. Retornará `204 No Content` em caso de sucesso.

## `DELETE /users/:userId`
Apaga o usuário informado. Retornará `204 No Content` em caso de sucesso.

## `GET /stats`
Retorna estatísticas globais do sistema.
```
{
    // Quantidade de hits em todas as urls do sistema
    "hits": 193841,
    // Quantidade de urls cadastradas
    "urlCount": 2512,
    // 10 Urls mais acessadas
    "topUrls": [
        // objeto stat ordernado por hits decrescente
        {
            "id": "23094",
            "hits": 153,
            "url": "http://www.example.com/blog",
            "shortUrl": "http://<host>[:<port>]/asdfeiba"
        },
        {
            "id": "23090",
            "hits": 89,
            "url": "http://www.example.com/rss",
            "shortUrl": "http://<host>[:<port>]/asdfeibb"
        },
        // ...
    ]
}
```

## `GET /stats/:id`

Retorna estatísticas de uma URL específica
``` 
{
    // ID da url
    "id": "23094",
    // Quantidade de hits nela
    "hits": 0,
    // A url original
    "url": "http://www.example.com/blog",
    // A url curta formada
    "shortUrl": "http://<host>[:<port>]/asdfeiba"
}
```