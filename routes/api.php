<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/stats', 'StatsController@index');
Route::get('/{id}', 'URLController@show')->name('hit.url');
Route::post('/users', 'UserController@store');
Route::post('/users/{userId}/urls', 'URLController@store');
Route::get('/users/{userId}/stats', 'StatsController@showByUser');
Route::delete('/urls/{id}', 'URLController@destroy');
Route::delete('/users/{userId}', 'UserController@destroy');
Route::get('/stats/{id}', 'StatsController@show');