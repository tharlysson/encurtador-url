ARG img
FROM webdevops/php-apache:7.4

EXPOSE 80

WORKDIR /app

COPY ./ /app

RUN composer install
RUN chmod 777 /app/.env
RUN chmod 777 -R /app/storage
RUN chmod 777 -R /app/public
RUN chmod 777 -R /app/storage/logs