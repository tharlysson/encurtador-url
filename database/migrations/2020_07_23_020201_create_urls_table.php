<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrlsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('urls', function (Blueprint $table) {
      $table->uuid('id')->primary();
      $table->string('user');
      $table->foreign('user', 'fk_user_url_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
      $table->unsignedBigInteger('hits');
      $table->string('shortUrl', 100);
      $table->string('url', 200);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('urls');
  }
}
