<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class StoreUserRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'id' => 'required|unique:users|max:255',
    ];
  }

  /**
   * Mensagens persolnalizadas para as validações
   *
   * @return array
   */
  public function messages()
  {
    return [];
  }

  /**
   * Retorna os erros encontrados
   *
   * @param Validator $validator
   */
  protected function failedValidation(Validator $validator)
  {
    if (isset($validator->failed()['id']['Unique'])) {
      throw new HttpResponseException(response(null, Response::HTTP_CONFLICT));
    }
    throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
  }
}
