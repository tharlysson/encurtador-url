<?php

namespace App\Providers;

use App\Models\Url;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
  /**
   * The event listener mappings for the application.
   *
   * @var array
   */
  protected $listen = [
    Registered::class => [
      SendEmailVerificationNotification::class,
    ],
  ];

  /**
   * Register any events for your application.
   *
   * @return void
   */
  public function boot()
  {
    parent::boot();

    Event::listen(RouteMatched::class, function ($route) {
      if ($route->route->getName() == 'hit.url') {
        $url = Url::query()->where('shortUrl', $route->route->id)->first();
        if ($url) {
          $url->update([
            'hits' => $url->hits + 1
          ]);
        }
      }
    });
  }
}
