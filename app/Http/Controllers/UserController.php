<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\Response;

class UserController extends Controller
{
  public function store(StoreUserRequest $request)
  {
    try {
      User::query()->create($request->only('id'));
      return response(null, Response::HTTP_CREATED);
    } catch (Exception $e) {
      return response($e->getMessage(), Response::HTTP_BAD_REQUEST);
    }
  }

  public function destroy($id)
  {
    try {
      $user = User::with(['urls'])->findOrFail($id);
      $user->delete();
      return response(null, Response::HTTP_NO_CONTENT);
    } catch (Exception $e) {
      return response(null, Response::HTTP_NOT_FOUND);
    }
  }
}