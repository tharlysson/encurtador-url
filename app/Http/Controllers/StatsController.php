<?php

namespace App\Http\Controllers;

use App\Models\Url;
use App\Models\User;
use Exception;
use Illuminate\Http\Response;

class StatsController extends Controller
{
  private const LIMIT = 10;

  public function index()
  {
    $response = [
      'hits' => Url::query()->sum('hits'),
      'urlCount' => Url::query()->count(),
      'topUrls' => Url::query()
        ->select(['id', 'hits', 'url', 'shortUrl'])
        ->orderBy('hits', 'desc')
        ->limit(self::LIMIT)
        ->get()
    ];
    return response($response, Response::HTTP_OK);
  }

  public function show($id)
  {
    try {
      $url = Url::query()->select(['id', 'hits', 'url', 'shortUrl'])->where('id', $id)->firstOrFail();
      return response($url, Response::HTTP_OK);
    } catch (Exception $e) {
      return response(null, Response::HTTP_NOT_FOUND);
    }
  }

  public function showByUser($userId)
  {
    try {
      User::query()->findOrFail($userId);
      $urls = Url::query()->select(['id', 'hits', 'url', 'shortUrl'])->where('user', $userId)->get();
      return response($urls, Response::HTTP_OK);
    } catch (Exception $e) {
      return response(null, Response::HTTP_NOT_FOUND);
    }
  }
}