<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreURLRequest;
use App\Models\Url;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class URLController extends Controller
{
  public function show($id)
  {
    try {
      $url = Url::query()->where('shortUrl', $id)->firstOrFail();
      return Redirect::to($url->url, Response::HTTP_MOVED_PERMANENTLY);
    } catch (Exception $e) {
      return response(null, Response::HTTP_NOT_FOUND);
    }
  }

  public function store(StoreURLRequest $request, $userId)
  {
    try {
      $response = Url::query()->create(['url' => $request->url, 'user' => $userId]);
      return response($response, Response::HTTP_CREATED);
    } catch (Exception $e) {
      return response($e->getMessage(), Response::HTTP_BAD_REQUEST);
    }
  }

  public function destroy($id)
  {
    try {
      $url = Url::query()->findOrFail($id);
      $url->delete();
      return response(null, Response::HTTP_NO_CONTENT);
    } catch (Exception $e) {
      return response(null, Response::HTTP_NOT_FOUND);
    }
  }
}