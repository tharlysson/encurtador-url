<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  public $keyType = 'string';
  public $incrementing = false;
  public $timestamps = false;
  protected $table = 'users';
  protected $fillable = ['id'];

  public function urls()
  {
    return $this->hasMany('App\Models\Url', 'user');
  }

  public static function boot()
  {
    parent::boot();

    static::deleting(function ($model) {
      $model->urls()->delete();
    });
  }
}