<?php

namespace App\Models;

use App\Utils\Shortener;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid as UuidGenerate;

class Url extends Model
{
  public $keyType = 'string';
  public $incrementing = false;
  public $timestamps = false;
  protected $table = 'urls';
  protected $fillable = [
    'id',
    'user',
    'hits',
    'shortUrl',
    'url',
  ];

  public static function boot()
  {
    parent::boot();

    self::creating(function ($model) {
      $model->id = UuidGenerate::uuid4();
      $model->hits = 0;
      $model->shortUrl = Shortener::generateRandomString();
    });

    self::created(function ($model){
      unset($model->user);
      $model->shortUrl = Shortener::mountURL($model->shortUrl);
    });

    self::retrieved(function ($model) {
      $model->shortUrl = Shortener::mountURL($model->shortUrl);
    });

    self::updating(function ($model) {
      $model->shortUrl = Shortener::unmountURL($model->shortUrl);
    });
  }
}