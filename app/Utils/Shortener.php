<?php

namespace App\Utils;

/**
 * Trecho de código retirado do site abaixo
 *
 * Class to create short URLs and decode shortened URLs
 *
 * @author CodexWorld.com <contact@codexworld.com>
 * @copyright Copyright (c) 2018, CodexWorld.com
 * @url https://www.codexworld.com
 */
class Shortener
{
  protected static string $chars = "abcdfghjkmnpqrstvwxyz|ABCDFGHJKLMNPQRSTVWXYZ|0123456789";

  /**
   * Gera uma string random para o encurtador da url
   *
   * @param int $length
   * @return string
   */
  public static function generateRandomString(int $length = 6): string
  {
    $sets = explode('|', self::$chars);
    $all = '';
    $randString = '';

    foreach ($sets as $set) {
      $randString .= $set[array_rand(str_split($set))];
      $all .= $set;
    }

    $all = str_split($all);

    for ($i = 0; $i < $length - count($sets); $i++) {
      $randString .= $all[array_rand($all)];
    }

    $randString = str_shuffle($randString);

    return $randString;
  }

  /**
   * Monta a short url com o dominio da aplicação
   *
   * @param string $shortURL
   * @return string
   */
  public static function mountURL(string $shortURL): string
  {
    return env('APP_URL', 'http://localhost') . "/{$shortURL}";
  }

  /**
   * Retira o dominio caso tenha setado
   *
   * @param string $shortURL
   * @return string
   */
  public static function unmountURL(string $shortURL): string
  {
    $url = explode('/', $shortURL);
    return end($url);
  }
}